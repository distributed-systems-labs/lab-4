package com.nau.lab4;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.Random;
import java.util.concurrent.ForkJoinPool;

public class Lab extends Application {
    private final Button submit = new Button("Вычислить");
    private final CheckBox[] checkboxes = new CheckBox[18];
    private final NumberTextField arraySizeField = new NumberTextField();
    private final NumberTextField threadsAmountField = new NumberTextField();
    private TextArea screen;
    private int[] array;


    private CheckBox[] buildCheckboxes() {
        int cores = Runtime.getRuntime().availableProcessors();
        for (int i = 0; i < checkboxes.length; i++) {
           checkboxes[i] = new CheckBox("" + (i + 1));

           if(i >= cores) {
               checkboxes[i].setDisable(true);
           }
        }
        checkboxes[0].setSelected(true);

        return checkboxes;
    }

    private GridPane buildCoresPane() {
        GridPane gp = new GridPane();
        gp.setHgap(20);
        gp.setVgap(15);
        gp.setLayoutX(40);
        gp.setLayoutY(50);

        CheckBox[] cBoxes = buildCheckboxes();

        gp.add(cBoxes[0], 0, 0);
        gp.add(cBoxes[1], 0, 1);
        gp.add(cBoxes[2], 0, 2);

        gp.add(cBoxes[3], 1, 0);
        gp.add(cBoxes[4], 1, 1);
        gp.add(cBoxes[5], 1, 2);

        gp.add(cBoxes[6], 2, 0);
        gp.add(cBoxes[7], 2, 1);
        gp.add(cBoxes[8], 2, 2);

        gp.add(cBoxes[9], 3, 0);
        gp.add(cBoxes[10], 3, 1);
        gp.add(cBoxes[11], 3, 2);

        gp.add(cBoxes[12], 4, 0);
        gp.add(cBoxes[13], 4, 1);
        gp.add(cBoxes[14], 4, 2);

        gp.add(cBoxes[15], 5, 0);
        gp.add(cBoxes[16], 5, 1);
        gp.add(cBoxes[17], 5, 2);

        return gp;
    }

    private GridPane buildInputPane() {
        GridPane gp = new GridPane();
        gp.setHgap(20);
        gp.setVgap(20);
        gp.setLayoutX(40);
        gp.setLayoutY(180);

        arraySizeField.setPrefSize(150, 15);
        Text sizeFieldText = new Text("Paзмер массива:");
        sizeFieldText.setFill(Color.BROWN);
        sizeFieldText.setFont(Font.font("Arial", FontWeight.BOLD, 12));

        threadsAmountField.setPrefSize(150, 15);
        Text threadsAmountText = new Text("Кол-во потоков:");
        threadsAmountText.setFill(Color.BROWN);
        threadsAmountText.setFont(Font.font("Arial", FontWeight.BOLD, 12));

        submit.setTextFill(Color.TOMATO);
        submit.setFont(Font.font("Arial", FontWeight.BOLD, 12));

        gp.add(sizeFieldText, 0, 0);
        gp.add(arraySizeField, 1, 0);
        gp.add(threadsAmountText, 0, 1);
        gp.add(threadsAmountField, 1, 1);

        gp.add(submit, 1, 2);
        return gp;
    }

    private TextArea buildTextArea() {
        screen = new TextArea();
        screen.setLayoutX(360);
        screen.setLayoutY(50);
        screen.setPrefSize(300, 280);
        screen.setFont(Font.font("Arial", FontPosture.ITALIC, 12));
        screen.setWrapText(true);
        return screen;
    }

    private Text buildTitle() {
        Text title = new Text("Доступные процессоры");
        title.setFont(Font.font("Arial", FontWeight.BOLD, 16));
        title.setFill(Color.BLUE);
        title.setLayoutX(40);
        title.setLayoutY(30);
        return title;
    }

    private Pane buildRootPane() {
        Pane mainPain = new Pane();
        TextArea textArea = buildTextArea();
        Text title = buildTitle();
        GridPane coresPane = buildCoresPane();
        GridPane inputsPane = buildInputPane();
        mainPain.getChildren().addAll(title, coresPane, textArea, inputsPane);
        return mainPain;
    }

    private void print(String text) {
        screen.appendText("\n" + text);
    }
    
    private void buildArray(int size) {
        array = new int[size];
        Random generator = new Random(19580427);
        for (int i = 0; i < size; i++) {
            array[i] = generator.nextInt(50000);
        }
    }

    private void setActions() {
        submit.setOnAction((ActionEvent event) -> {
            byte cores = 0;
            for (CheckBox checkbox : checkboxes) {
                if (checkbox.isSelected()) cores++;
            }
            if (cores > 0) {
                print("Выбрано процессоров: " + cores);

                int arraySize = arraySizeField.getFieldValue();
                print("Размер массива: " + arraySize);
                buildArray(arraySize);
                int threadsCount = threadsAmountField.getFieldValue();
                print("Кол-во потоков: " + threadsCount);
                int threshold = arraySize / threadsCount;

                long startTime = System.currentTimeMillis();

                int max = new ForkJoinPool(threadsCount).invoke(new FindMaxTask(array, 0, array.length, threshold));

                long endTime = System.currentTimeMillis();
                print("Максимальное значение: " + max);
                print("Время выполнения: " + (endTime - startTime) + "ms");
            }
        });
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Лабораторная работа №4. Кузнєцов Олексій");
        primaryStage.setResizable(false);

        Pane rootPane = buildRootPane();

        Scene scene = new Scene(rootPane, 700, 360, Color.TRANSPARENT);
        primaryStage.setScene(scene);
        primaryStage.show();

        setActions();
    }

    public static void main(String[] args) {
        launch(args);
    }

}

package com.nau.lab4;

import java.util.concurrent.RecursiveTask;

public class FindMaxTask extends RecursiveTask<Integer> {
    private final int start;
    private final int end;
    private final int threshold;
    private final int[] array;

    public FindMaxTask(int[] array, int start, int end, int threshold) {
        this.start = start;
        this.end = end;
        this.threshold = threshold;
        this.array = array;
    }

    @Override
    protected Integer compute() {
        int length = end - start;
        if (length < threshold) {
            return computeDirectly();
        }

        int offset = length / 2;

        FindMaxTask left = new FindMaxTask(array, start, start + offset, threshold);
        left.fork();
        FindMaxTask right = new FindMaxTask(array, start + offset, end, threshold);

        return Math.max(right.compute(), left.join());
    }

    private Integer computeDirectly() {
        int max = Integer.MIN_VALUE;
        for (int i = start; i < end; i++) {
            if (max < array[i]) {
                max = array[i];
            }
        }
        return max;
    }
}
